package com.manning.gia.todo.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

import com.manning.gia.todo.model.ToDoItem;

public class InMemoryToDoRepository implements ToDoRepository {

	private AtomicLong currentId = new AtomicLong();
	private ConcurrentMap<Long, ToDoItem> toDos = new ConcurrentHashMap<>();

	@Override
	public List<ToDoItem> findAll() {
		List<ToDoItem> doItems = new ArrayList<>(toDos.values());
		Collections.sort(doItems);
		return doItems;
	}

	@Override
	public ToDoItem findById(Long id) {
		return toDos.get(id);
	}

	@Override
	public Long insert(ToDoItem doItem) {
		Long id = currentId.incrementAndGet();
		doItem.setId(id);
		toDos.putIfAbsent(id, doItem);
		return id;

	}

	@Override
	public void update(ToDoItem doItem) {
		toDos.replace(doItem.getId(), doItem);

	}

	@Override
	public void delete(ToDoItem doItem) {
		toDos.remove(doItem.getId());

	}

}
